--
-- this lua code is run separatedly, but get commands from tcp socket
--
--

print("editor-csg");

--set path for lua modules
package.path='/usr/local/lib/lua/5.3/?.lua;'..package.path
package.cpath='/usr/local/lib/lua/5.3/?.so;'..package.cpath
print(package.path)
print(package.cpath)


ocsg=require("opencsg")
nef=require("nef")


--
-- communication par socket
--
socket = require("socket")
server = assert(socket.bind("*", 51515))
print(socket._VERSION)





-- toutes les commandes sont ici
luacad=require("luacad")

wid=ocsg.init()
print("lua wid is ",wid)

--
--
-- gp est une liste dont les elements sont
-- {op="+"|"*"|"-",nef=[le nef],hash=[le hash]}
--
--
function drawDisplayProds(gp)
    ocsg.clear()
    local op
    -- remplace le premier '+' par '*'
    for i,v in ipairs(gp) do
        if v.op=="+" then
            print(">>new product")
            ocsg.newProduct()
            op="*"
        else
            op=v.op
        end
        print(">> draw ",op," h=",v.hash, "nef=",v.nef)
        ocsg.draw(op,v.nef)
    end
end


renderer = coroutine.create(
    function()
		::loop::
		    k=coroutine.yield()
            if k then
                print("Got new geometry!!!")
                --local t=os.clock()
                drawDisplayProds(k)
                --print("draw ",math.floor((os.clock()-t)*1000),"ms")
            end
            --local t=os.clock()
            ocsg.render()
            --`print("render ",math.floor((os.clock()-t)*1000),"ms")
            --print(i)
		goto loop
    end
)

-------------------------
function boucle()
    if displayProducts then
        drawDisplayProds(displayProducts)
        displayProducts=nil
    end
    ocsg.render()
end

-------------------------
while 1 do

  print("waiting for cconnect")
  local client = server:accept()
  client:settimeout(0.1,"b")

  while 1 do 
      print("wait...")
      line,err = client:receive("*l")
      if not line then
          if err=="closed" then break end
      else
          print("got '"..tostring(line).."' and err="..tostring(err))
          k,err=load(line)
          if k then
            res=k()
            client:send("* "..tostring(res).."\n")
          else
            client:send("! "..err.."\n")
          end
      end
      boucle()
  end

end

--------------------------


--return wid

--while true do coroutine.resume(renderer,i) end
--for i = 1, 100000 do coroutine.resume(renderer,i) end


