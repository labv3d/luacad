local socket = require("socket")
local host, port = "127.0.0.1", 51515
local tcp = assert(socket.tcp())

tcp:connect(host, port);
tcp:send("hello world\n");

tcp:send("a=5\nb=6\nreturn a+b\n");

while true do
    local s, status, partial = tcp:receive()
    print(s or partial)
    if status == "closed" then
      break
    end
end

tcp:close()

